#
# Module: RESTful
#
# Description: A set of functions to access a RESTful interface
#

from __future__ import print_function

CDATA_BEGIN='' #'<![CDATA['
CDATA_END='' #']]>'
DEBUG_SEPARATOR = '--------'
HEADERS = {'Content-Type': 'application/xml',
           'Accept': 'application/xml'}

import sys
sys.path.append('.')

# This code is needed is pyxml if installed
pyxml=None
index = 0
for p in sys.path:
    if -1 != p.find('pyxml'):
         pyxml = p
    index += 1
if None != pyxml:
    sys.path.remove(pyxml)

import xml.etree.ElementTree as ET


class FatalError(Exception):
    """Captures detail of a fatal error in response to a RESTful request"""

    def __init__(self, message, errorCode, response):
        self.code = errorCode
        self.message = message
        self.response = response


def _append_cdata(element, name, cdata):
    """ Appends a CDATA child element with the supplied name to the supplied element
    """

    if None == cdata:
        return
    child = ET.Element(name)
    child.text = CDATA_BEGIN + cdata + CDATA_END
    element.append(child)


def _check_status(url, r, expected):
    """Checks the return status of a request to a URL

    Keyword arguments:
    url      -- the URL to which the request was made
    r        -- the response to the request
    expected -- the expected response code
    """
    if expected == r.status_code:
        return
    elif 400 == r.status_code:
        raise FatalError('Application at "' + url  + '" can not process this request as it is bad', r.status_code, r.text)
    elif 401 == r.status_code:
        raise FatalError('Not authorized to execute commands for Application at "' + url, r.status_code, r.text)
    elif 404 == r.status_code:
        raise FatalError('Application at "' + url  + '" not found', r.status_code, r.text)
    raise FatalError('Unexpected status (' + str(r.status_code) + ') returned from "' + url  + '"', r.status_code, r.text)

def _debug_separator():
    _eprint(DEBUG_SEPARATOR)


def _eprint(*args, **kwargs):
    """Prints to standard error"""
    print(*args, file=sys.stderr, **kwargs)


def _pretty_print(debug, url, s, response = True):
    """Prints out a formatted version of the supplied XML

    :param boolean debug: True it the output should be written.
    :param str url: the URL to which the request was made.
    :param str s: the XML to print.
    :param bool response: True is the XML is the reponse to a request.
    """
    if debug:
        if None != url:
            if response:
                _eprint('URL : Response : ' + url)
            else:
                _eprint('URL : Request :  ' + url)
        _eprint(xml.dom.minidom.parseString(s).toprettyxml())
        _debug_separator()



class RESTful(object):
    def __init__(self, service, url, xml = False, cert = None, key = None, cacert = None, verify = True):
        """An Object that talks to a RESTfil server.

        :param str service: the name to use in place of <service>.
        :param str url: the URL of the REST instance.
        :param bool xml: True if the raw XML exchanges should be logged (default: False).
        :param str cert: path to the file containing the client\'s x509
            certificate (default: None, which implies
            ``${HOME}/.<service>/client/cert/<service>_client.pem`` if it exists).
        :param str key: path to the file containing path to the client\'s
            private x509 key (default: None, which implies
            ``${HOME}/.<service>/client/private/<service>_client.key`` if it exists).
        :param str cacert: path to the file containing one or more CA x509
            certificates (default: None, which implies
            ``${HOME}/.<service>/client/cert/cacert.pem`` if it exists).
        :param boolean verify: controls whether the server’s TLS certificate is verified
            (default : True). 

        The ``cert`` and ``key`` will only be used if the files
        containing them exist, otherwise they are ignored.

        The alternate ``cacert`` location is only used if the specified
        directory exists.
        """

        self.service = service
        self.url=url
        self.debug=xml
        self.session=requests.Session()
        client_dir=os.getenv('HOME') + '/.' + service + '/client'
        if None == cert:
            cert = client_dir + '/cert/' + service + '_client.pem' #Client certificate
        if None == key:
            key = client_dir + '/private/' + service + '_client.key' #Client private key
        if None == cacert:
            cacert = client_dir + '/cert/cacert.pem' #CA certificate file
        if os.path.exists(cert) and os.path.exists(key):
            self.session.cert = (cert, key)
        if os.path.exists(cacert):
            self.session.cacert = cacert
        if None != verify:
            self.session.verify = verify


    def debug_separator(self):
        _eprint(DEBUG_SEPARATOR)


    def get_application(self):
        """:return: the application document at the URL
        :rtype: ElementTree

        :raises FatalError: if the server response in not OK. 
        """

        try:
            r = self.session.get(self.url)
            _check_status(self.url, r, 200)
            application = ET.fromstring(r.text)
            self._pretty_print(self.url, ET.tostring(application))
            return application
        except requests.exceptions.ConnectionError as e:
           raise FatalError('Unable to conenct to application at "' + self.url + '"', None, e)


    def get_named_resource(self, application, xpath, name):
        """
        Returns the a Named Resource with a group of Named Resources

        :param ElementTree application: the ElementTree containing the Named Resource Groups.
        :param str xpath: the xpath to the Named Resources Group that contains the Named Resource.
        :param str name: the name of the Named Resource to be returned.

        :return: the URI of a Named Resource.
        :rtype: str
        :raises FatalError: if the server response in not OK.
        """

        named = application.find(xpath + '/[name="' + name + '"]')
        if None == named:
            raise FatalError('This version of the Catalog server does not support the "' + name + '" command', 2, ET.tostring(application))
        return named


    def pretty_print(self, url, s, response = True):
        """Prints out a formatted version of the supplied XML

        :param str url: the URL to which the request was made.
        :param str s: the XML to print.
        :param bool response: True is the XML is the reponse to a request.
        """
        _pretty_print(self.debug, url, s, response)


class ResourceAccess:
    """
    This class defined the mechanics of accessing a Named Resource.
    
    :param Session session: the "requests" Session instance managing access to the application.
    :param bool xml: True if the raw XML exchanges should be logged (default: False).
    """
    def __init__(self, session, xml = False):
        self.debug = xml
        self.session = session


    def command(self, named):
        """
        Accesses a command Named Resource, i.e. one that may cause changed in the application.
        
        :param ElementTree named the ElementTree of the Named Resource.
        """
        if None == self.session:
            return
        named_uri = named.find('uri').text
        attach = named.find('attachment')
        if None != attach:
            media_type = attach.text
            document = self._prepare_document(media_type)
            self._pretty_print(named_uri, ET.tostring(document), False)
            r = self.session.post(named_uri, data=ET.tostring(document), headers=HEADERS)
        else:
            r = self.session.post(named_uri, headers=HEADERS)
        try:
            _check_status(named_uri, r, 200)
        except FatalError as e:
            # Allow for "received" return code
            if 202 != e.code:
                raise e
        response = ET.fromstring(r.text)
        self._pretty_print(named_uri, r.text, True)
        self._display_response(named,
                               response)


    def debug_separator(self):
        _eprint(DEBUG_SEPARATOR)


    def _display_response(self,
                          named,
                          response):
        """
        Displays the result of accessing a Named Resource
 
        :param ElementTree named the ElementTree of the Named Resource.
        :param ElementTree response the ElementTree containing the response to access in the Named Resource.
        """
        raise FatalError('The "_display_report" function has not been defined for this class', -5, None)


    def _prepare_document(self, media_type):
        """
        Prepares a Bundles document contains the specified bundle identities
        
        :param str media_type a string definition the media type of the document to be prepared.
        """
        raise FatalError('The "_prepare_document" function has not been defined for this class', -5, None)


    def _prepare_query(self):
        return None


    def pretty_print(self, url, s, response = True):
        """Prints out a formatted version of the supplied XML

        :param str url: the URL to which the request was made.
        :param str s: the XML to print.
        :param bool response: True is the XML is the reponse to a request.
        """
        _pretty_print(self.debug, url, s, response)


    def report(self, named):
        """
        Accesses a reporting Named Resource, i.e. one that does not cause changed in the application.

        :param ElementTree named the ElementTree of the Named Resource.
        """
        if None == self.session:
            return
        named_uri = named.find('uri').text
        query = self._prepare_query()
        if None == query:
            uri_to_use = named_uri
        else:
            uri_to_use = named_uri + '?' + query
        attach = named.find('attachment')
        if None != attach:
            media_type = attach.text
            document = self._prepare_document(media_type)
            self._pretty_print(uri_to_use, ET.tostring(document), False)
            # Due to a "feature" of Wildfly, GET methods can not receive a document
            #   Therefore, at present, POST a report that requires a document
            r = self.session.post(uri_to_use, data=ET.tostring(document), headers=HEADERS)
        else:
            r = self.session.get(uri_to_use, headers=HEADERS)
        _check_status(uri_to_use, r, 200)
        response = ET.fromstring(r.text)
        self._pretty_print(named_uri, r.text, True)
        self._display_response(named,
                              response)
