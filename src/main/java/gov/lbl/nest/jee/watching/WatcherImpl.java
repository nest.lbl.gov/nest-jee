package gov.lbl.nest.jee.watching;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;

/**
 * This class is a default Watcher, bound the WatcherDB.
 * 
 * @author patton
 */
@Stateless
public class WatcherImpl extends
                         Watcher {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link EntityManager} instance used by this object.
     */
    @Inject
    @WatcherDB
    private EntityManager entityManager;

    // constructors

    // instance member method (alphabetic)

    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
