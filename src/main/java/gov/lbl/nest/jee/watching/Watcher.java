package gov.lbl.nest.jee.watching;

import java.util.Date;
import java.util.List;
import java.util.Set;

import gov.lbl.nest.common.watching.ChangedItem;
import gov.lbl.nest.common.watching.WatchedList;
import jakarta.persistence.EntityManager;
import jakarta.persistence.NoResultException;
import jakarta.persistence.TypedQuery;

/**
 * <p>
 * This pattern has been superseded by the {@link WatchedList} pattern.
 * </p>
 * <p>
 * The `Watcher` pattern is designed to watch a time ordered list as it grows.
 * The list is "watched" by making periodic requests for the next set of entries
 * and passing those entries onto whatever needs to process them. Thus, from the
 * `Watcher`'s point of view, this list appears to be broken up into a
 * collection of time order blocks of the original list, with each entries
 * appearing in one and only one block. (This means that the processing code
 * isn't required to detect repeated entries - the `Watcher` implementation
 * takes care of that.) Each entry returned is associated with a timestamp with
 * the last entry in a response being considered as the current high water mark
 * of the list from the the `Watcher`'s point of view.
 * </p>
 * <p>
 * There is a race condition that needs to dealt with by the `Watcher`, namely
 * the case where the response to a request contains only some entries that
 * occur in the list simultaneously. This can be cause by a number of reasons,
 * including the size of the response being limited so not all simultaneous
 * entries can be included, or the response was generated between two
 * simultaneous entries are added to the list. The `Watcher` pattern accounts
 * for this by keeping a list of all entries that occurred at the high water
 * mark. When the next request for a block of the list asks for all entries
 * **on** or after the high water mark. This means that the one or more of the
 * entries from the previous block will appear in the new response. It is the
 * `Watcher`'s responsibility, using the `forwardLastWatched` method, to trim
 * the response and return the block without any entries that occurred in the
 * previous block.
 * </p>
 * <p>
 * The following code example shows how a `Watcher` can be used to create a
 * `Digest` instance based in the latest block of entries. The `role` variable
 * is a unique `String` that is used to differentiate between different
 * `Watcher` instances. The `lastWatched` instance captures which `Watcher`
 * instance to use as well as the current high water mark for that instance. The
 * `getIssuedItemsOnOrAfter(Date)` method is provided by the user and returns
 * the time order list of entries on or after the provided high water `Date`.
 * The returned `issued` list is the resultant collection of entires that make
 * up the next block, i.e. entries that occurred in the previous block have been
 * removed.
 * </p>
 * 
 * <pre>
 *      final Digest digest;
 *      final LastWatched last = watcher.getLastWatched(role);
 *      if (null == last) {
 *          digest = null;
 *      } else {
 *          final Date lastDateTime = last.getLastDateTime();
 *          final List&lt;T extends ChangedItem&gt; items = getIssuedItemsOnOrAfter(lastDateTime);
 *          final List&lt;? extends ChangedItem&gt; issued = watcher.forwardLastWatched(last,
 *                                                                                items);
 *          digest = new Digest("Digest subject line",
 *                              issued);
 *      }
 * </pre>
 * 
 * @see WatcherImpl
 * 
 * @author patton
 */
public abstract class Watcher {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    /**
     * Forwards the supplied {@link LastWatched} instance to the last item in the
     * supplied list, and returns a list trimmed of any items already used by that
     * instance. This method does not limit the number of items returned.
     * 
     * @param last
     *            the {@link LastWatched} to forward.
     * @param items
     *            the set of items that have been seen since the supplied
     *            {@link LastWatched} instance.
     * @param <T>
     *            the class that is actually in the list.
     * 
     * @return the list of items not already used by the {@link LastWatched}.
     */
    public <T extends ChangedItem> List<T> forwardLastWatched(final LastWatched last,
                                                              final List<T> items) {
        return forwardLastWatched(last,
                                  items,
                                  0);
    }

    /**
     * Forwards the supplied {@link LastWatched} instance to the last item in the
     * supplied list, and returns a list trimmed of any items already used by that
     * instance. This method allows a limit to be be set on the number of items
     * returned.
     * 
     * @param last
     *            the {@link LastWatched} to forward.
     * @param items
     *            the set of items that have been seen since the supplied
     *            {@link LastWatched} instance.
     * @param maxCount
     *            the maximum number of items to return, 0 or less means there is no
     *            limit.
     * @param <T>
     *            the class that is actually in the list.
     * 
     * @return the list of items not already used by the {@link LastWatched}.
     */
    public <T extends ChangedItem> List<T> forwardLastWatched(final LastWatched last,
                                                              final List<T> items,
                                                              final int maxCount) {
        final List<T> result = WatcherUtils.sinceLastWatched(last,
                                                             items,
                                                             maxCount);
        if (result.isEmpty()) {
            return result;
        }

        // Update set of last items.
        int index = result.size() - 1;
        final ChangedItem lastItem = result.get(index);
        final Date lastItemDateTime = lastItem.getWhenItemChanged();
        final EntityManager entityManager = getEntityManager();
        Date lastDateTime = last.getLastDateTime();
        Set<SeenItem> lastItems = last.getLastItems();
        if (lastItemDateTime.getTime() > lastDateTime.getTime()) {
            for (SeenItem item : lastItems) {
                entityManager.remove(item);
            }
            lastItems.clear();
            lastDateTime = lastItem.getWhenItemChanged();
        }
        boolean done = false;
        while (index >= 0 && !done) {
            final ChangedItem item = result.get(index);
            index--;
            final Date itemDateTime = item.getWhenItemChanged();
            if (lastDateTime.getTime() > itemDateTime.getTime()) {
                done = true;
            } else {
                final SeenItem itemToAdd = new SeenItem(last,
                                                        item.getItem());
                entityManager.persist(itemToAdd);
                lastItems.add(itemToAdd);
            }
        }
        last.setLastDateTime(lastDateTime);
        return result;
    }

    /**
     * Returns the {@link EntityManager} instance used by this object.
     * 
     * @return the {@link EntityManager} instance used by this object.
     */
    abstract public EntityManager getEntityManager();

    /**
     * Returns the {@link LastWatched} for the specified role, creating it if it
     * does not exist.
     * 
     * @param role
     *            the role of the person or process whose {@link LastWatched}
     *            instance should be returned
     * 
     * @return the {@link LastWatched} for the specified role.
     */
    public LastWatched getLastWatched(String role) {
        final EntityManager entityManager = getEntityManager();
        final TypedQuery<LastWatched> query = entityManager.createNamedQuery("getLastWatched",
                                                                             LastWatched.class);
        query.setParameter("role",
                           role);
        try {
            return query.getSingleResult();
        } catch (NoResultException e) {
            final LastWatched last = new LastWatched(role,
                                                     new Date(0));
            entityManager.persist(last);
            return last;
        }
    }

    /**
     * Rewinds the specified {@link LastWatched} instance back to the provided date
     * and time. If this supplied data and time is later than the current
     * {@link LastWatched} instance's latest value then nothing is changed.
     * 
     * @param last
     *            the {@link LastWatched} instance to rewind.
     * @param dateTime
     *            the data and time to which to rewind it.
     * 
     * @return the data and time set for the {@link LastWatched} instances.
     */
    public Date rewindLastWatched(LastWatched last,
                                  Date dateTime) {
        if (dateTime.before(last.getLastDateTime())) {
            final EntityManager entityManager = getEntityManager();
            final TypedQuery<SeenItem> query = entityManager.createNamedQuery("itemsForLastWatched",
                                                                              SeenItem.class);
            query.setParameter("lastWatched",
                               last);
            final List<SeenItem> items = query.getResultList();
            if (null != items && !items.isEmpty()) {
                for (SeenItem item : items) {
                    entityManager.remove(item);
                }
            }
            last.setLastDateTime(dateTime);
        }
        return last.getLastDateTime();
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

}
// public static void main(String args[]) {}