package gov.lbl.nest.jee.watching;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import gov.lbl.nest.common.watching.ChangedItem;
import gov.lbl.nest.common.watching.WatchedList;
import gov.lbl.nest.common.watching.WatchedList.Callback;
import gov.lbl.nest.common.watching.WatchedListFactory;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.NoResultException;
import jakarta.persistence.TypedQuery;

/**
 * This implements the {@link WatchedListFactory} interface to return
 * {@link WatchedList} instances that are bound to the {@link WatcherDB}.
 * 
 * @author patton
 */
@Stateless
public class WatchedListFactoryImpl implements
                                    WatchedListFactory {

    /**
     * Returns a list trimmed of all items already used by that instance
     * <em>without</em> forwarding the supplied {@link LastWatched} instance.
     * 
     * @param last
     *            the {@link LastWatched} to from which to trim.
     * @param items
     *            the time ordered list of {@link ChangedItem} instances to trim.
     * @param <T>
     *            the class that is actually in the list.
     * 
     * @return the list trimmed of any items already used by that instance.
     */
    private static <T extends ChangedItem> List<T> sinceLastWatched(final LastWatched last,
                                                                    final List<T> items) {
        Date lastDateTime = last.getLastDateTime();
        Set<SeenItem> lastItems = last.getLastItems();
        final List<T> result = new ArrayList<T>(items.size());
        final int finished = items.size();
        for (int index = 0;
             finished != index;
             index++) {
            final T item = items.get(index);
            final Date itemDateTime = item.getWhenItemChanged();
            final int compare = lastDateTime.compareTo(itemDateTime);
            if (0 < compare) {
                // Skip this item as it should have already been used.
            } else if (0 == compare) {
                final SeenItem testItem = new SeenItem(item.getItem());
                if (!lastItems.contains(testItem)) {
                    result.add(item);
                }
            } else {
                /*
                 * All remaining items are after the current `lastDateTime` so add then all to
                 * the result and return.
                 */
                result.addAll(items.subList(index,
                                            items.size()));
                return result;
            }
        }
        return result;
    }

    /**
     * The {@link EntityManager} instance used by this object.
     */
    @Inject
    @WatcherDB
    private EntityManager entityManager;

    @Override
    public WatchedList createWatchedList(String role,
                                         Callback callback) {
        return new WatchedListImpl(this,
                                   role,
                                   callback);
    }

    /**
     * Forwards the supplied {@link LastWatched} instance to the last item in the
     * supplied list, and returns a list trimmed of any items already used by that
     * instance. This method does not limit the number of items returned.
     * 
     * @param last
     *            the {@link LastWatched} to forward.
     * @param items
     *            the set of items that have been seen since the supplied
     *            {@link LastWatched} instance.
     * @param <T>
     *            the class that is actually in the list.
     * 
     * @return the list of items not already used by the {@link LastWatched}.
     */
    <T extends ChangedItem> List<T> forwardLastWatched(final LastWatched last,
                                                       final List<T> items) {
        final List<T> result = sinceLastWatched(last,
                                                items);
        if (result.isEmpty()) {
            return result;
        }

        // Update set of last items.
        int index = result.size() - 1;
        final ChangedItem lastItem = result.get(index);
        final Date lastItemDateTime = lastItem.getWhenItemChanged();
        Date lastDateTime = last.getLastDateTime();
        Set<SeenItem> lastItems = last.getLastItems();
        if (lastItemDateTime.getTime() > lastDateTime.getTime()) {
            for (SeenItem item : lastItems) {
                entityManager.remove(item);
            }
            lastItems.clear();
            lastDateTime = lastItem.getWhenItemChanged();
        }
        boolean done = false;
        while (index >= 0 && !done) {
            final ChangedItem item = result.get(index);
            index--;
            final Date itemDateTime = item.getWhenItemChanged();
            if (lastDateTime.getTime() > itemDateTime.getTime()) {
                done = true;
            } else {
                final SeenItem itemToAdd = new SeenItem(last,
                                                        item.getItem());
                entityManager.persist(itemToAdd);
                lastItems.add(itemToAdd);
            }
        }
        last.setLastDateTime(lastDateTime);
        return result;
    }

    /**
     * Returns the {@link LastWatched} for the specified role, creating it if it
     * does not exist.
     * 
     * @param role
     *            the role of the person or process whose {@link LastWatched}
     *            instance should be returned
     * 
     * @return the {@link LastWatched} for the specified role.
     */
    LastWatched getLastWatched(String role) {
        final TypedQuery<LastWatched> query = entityManager.createNamedQuery("getLastWatched",
                                                                             LastWatched.class);
        query.setParameter("role",
                           role);
        try {
            return query.getSingleResult();
        } catch (NoResultException e) {
            final LastWatched last = new LastWatched(role,
                                                     new Date(0));
            entityManager.persist(last);
            return last;
        }
    }
}
