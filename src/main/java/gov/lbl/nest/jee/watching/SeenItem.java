package gov.lbl.nest.jee.watching;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.xml.bind.annotation.XmlElement;

/**
 * This class represents an item that has already been seen as part of a
 * {@link LastWatched} instance.
 * 
 * @author patton
 */
@Entity
@NamedQueries({ @NamedQuery(name = "itemsForLastWatched",
                            query = "SELECT i" + " FROM SeenItem i"
                                    + " WHERE lastWatched = :lastWatched") })
public class SeenItem {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The identity of the item this object represents.
     */
    private String item;

    /**
     * This object's unique identifier within the set of {@link SeenItem} instances.
     */
    private int seenItemKey;

    /**
     * The {@link LastWatched} instance to which this object belongs.
     */
    private LastWatched lastWatched;

    // constructors

    /**
     * Create an instance of this class.
     */
    protected SeenItem() {
    }

    /**
     * Create an instance of this class.
     * 
     * @param last
     *            the {@link LastWatched} instance to which this object belongs.
     * @param item
     *            the identity of the item this object represents.
     */
    protected SeenItem(LastWatched last,
                       String item) {
        setLastWatched(last);
        setItem(item);
    }

    /**
     * Create an instance of this class. Note this instance can not be persisted
     * until its {@link LastWatched} is set.
     * 
     * @param item
     *            the identity of the item this object represents.
     */
    protected SeenItem(String item) {
        setItem(item);
    }

    // instance member method (alphabetic)

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SeenItem other = (SeenItem) obj;
        if (getItem() == null) {
            if (other.getItem() != null)
                return false;
        } else if (!getItem().equals(other.getItem()))
            return false;
        return true;
    }

    /**
     * Returns the identity of the item this object represents.
     * 
     * @return the identity of the item this object represents.
     */
    @XmlElement
    @Column(length = 256,
            nullable = false)
    protected String getItem() {
        return item;
    }

    /**
     * Returns the {@link LastWatched} instance to which this object belongs.
     * 
     * @return the {@link LastWatched} instance to which this object belongs.
     */
    @ManyToOne(optional = false)
    protected LastWatched getLastWatched() {
        return lastWatched;
    }

    /**
     * Returns this object's unique identifier within the set of {@link SeenItem}
     * instances.
     * 
     * @return this object's unique identifier within the set of {@link SeenItem}
     *         instances.
     */
    @Id
    @GeneratedValue
    protected int getSeenItemKey() {
        return seenItemKey;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getItem() == null) ? 0
                                                       : getItem().hashCode());
        return result;
    }

    /**
     * Sets the identity of the item this object represents.
     * 
     * @param item
     *            the identity of the item this object represents.
     */
    protected void setItem(String item) {
        this.item = item;
    }

    /**
     * Sets the {@link LastWatched} instance to which this object belongs.
     * 
     * @param last
     *            the {@link LastWatched} instance to which this object belongs.
     */
    protected void setLastWatched(LastWatched last) {
        lastWatched = last;
    }

    /**
     * Sets this object's unique identifier within the set of {@link SeenItem}
     * instances.
     * 
     * @param key
     *            the unique value
     */
    protected void setSeenItemKey(int key) {
        seenItemKey = key;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
