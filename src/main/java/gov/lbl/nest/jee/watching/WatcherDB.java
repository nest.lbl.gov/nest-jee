package gov.lbl.nest.jee.watching;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import jakarta.inject.Qualifier;

/**
 * This annotation is used to qualify the Watcher Database access.
 * 
 * @author patton
 */
@Qualifier
@Retention(RUNTIME)
@Target({ TYPE,
          METHOD,
          FIELD,
          PARAMETER })
public @interface WatcherDB {
    // Does nothing, just a label.
}
