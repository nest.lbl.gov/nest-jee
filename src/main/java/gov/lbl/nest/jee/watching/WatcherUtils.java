package gov.lbl.nest.jee.watching;

import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import gov.lbl.nest.common.watching.ChangedItem;
import gov.lbl.nest.common.watching.Digest;
import jakarta.mail.Message;
import jakarta.mail.Session;
import jakarta.mail.Transport;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeMessage;
import jakarta.ws.rs.core.Response;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.Marshaller;

/**
 * This class provides utilities to be used with a {@link Watcher} instance.
 * 
 * @author patton
 */
public abstract class WatcherUtils {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    /**
     * The http response code for OK.
     */
    private static final int HTTP_STATUS_OK = 200;

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    /**
     * Post a {@link Digest} instance to the URL.
     * 
     * @param digest
     *            the {@link Digest} instance to be posted.
     * @param url
     *            the {@link URL} instance to which the digest should be posted.
     * 
     * @throws WatchingException
     *             when the digest can not be successfully POSTed.
     */
    public static void post(Digest digest,
                            URL url) throws WatchingException {
        try {
            final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            if (null != digest) {
                connection.setRequestMethod("POST");
                connection.setDoOutput(true);
            }
            connection.setRequestProperty("Accept",
                                          "application/xml");
            connection.setRequestProperty("Content-Type",
                                          "application/xml");
            connection.connect();
            if (null != digest) {
                JAXBContext context = JAXBContext.newInstance(digest.getClass());
                Marshaller marshaller = context.createMarshaller();
                marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,
                                       true);
                try (final OutputStream os = connection.getOutputStream()) {
                    marshaller.marshal(digest,
                                       os);
                }
            }
            if (HTTP_STATUS_OK != connection.getResponseCode()) {
                throw new IOException("HTTP response was " + connection.getResponseCode()
                                      + ", not "
                                      + Response.Status.OK.getStatusCode()
                                      + " that is the expected value");
            }
        } catch (Exception e) {
            throw new WatchingException(e);
        }
    }

    /**
     * Sends a {@link Digest} instance to the specified email addresses using the
     * supplied {@link Session}.
     * 
     * @param digest
     *            the {@link Digest} instance to be sent.
     * @param session
     *            the {@link Session} used to send this object.
     * @param recipients
     *            the addresses to send this object.
     * 
     * @throws WatchingException
     *             when the digest can not be successfully sent.
     */
    public static void send(Digest digest,
                            Session session,
                            List<String> recipients) throws WatchingException {
        if (null == session) {
            return;
        }
        try {
            JAXBContext context = JAXBContext.newInstance(Digest.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,
                                   true);
            final StringWriter writer = new StringWriter();
            marshaller.marshal(digest,
                               writer);
            final Message msg = new MimeMessage(session);
            for (String recipient : recipients) {
                msg.addRecipient(Message.RecipientType.TO,
                                 new InternetAddress(recipient));
            }
            msg.setSubject("\"" + digest.getSubject()
                           + "\" digest");
            msg.setContent(writer.toString(),
                           "text/xml");
            Transport transport = session.getTransport();
            transport.connect();
            transport.sendMessage(msg,
                                  msg.getAllRecipients());
            transport.close();
        } catch (Exception e) {
            throw new WatchingException(e);
        }
    }

    /**
     * Sends a {@link Digest} instance to the specified email address using the
     * supplied {@link Session}.
     * 
     * @param digest
     *            the {@link Digest} instance to be sent.
     * @param session
     *            the {@link Session} used to send this object.
     * @param recipient
     *            the address to send this object.
     * 
     * @throws WatchingException
     *             when the digest can not be successfully sent.
     */
    public static void send(Digest digest,
                            Session session,
                            String recipient) throws WatchingException {
        final List<String> list = new ArrayList<String>(1);
        list.add(recipient);
        send(digest,
             session,
             list);
    }

    /**
     * Returns a list trimmed of all items already used by that instance
     * <em>without</em> forwarding the supplied {@link LastWatched} instance. This
     * method does not limit the number of items returned.
     * 
     * @param last
     *            the {@link LastWatched} to from which to trim.
     * @param items
     *            the time ordered list of {@link ChangedItem} instances to trim.
     * @param <T>
     *            the class that is actually in the list.
     * 
     * @return the list trimmed of any items already used by that instance.
     */
    public static <T extends ChangedItem> List<T> sinceLastWatched(final LastWatched last,
                                                                   final List<T> items) {
        return sinceLastWatched(last,
                                items,
                                0);
    }

    /**
     * Returns a list trimmed of all items already used by that instance
     * <em>without</em> forwarding the supplied {@link LastWatched} instance. This
     * method allows a limit to be be set on the number of items returned.
     * 
     * @param last
     *            the {@link LastWatched} to from which to trim.
     * @param items
     *            the time ordered list of {@link ChangedItem} instances to trim.
     * @param maxCount
     *            the maximum number of items to return, 0 or less means there is no
     *            limit.
     * @param <T>
     *            the class that is actually in the list.
     * 
     * @return the list trimmed of any items already used by that instance.
     */
    public static <T extends ChangedItem> List<T> sinceLastWatched(final LastWatched last,
                                                                   final List<T> items,
                                                                   final int maxCount) {
        Date lastDateTime = last.getLastDateTime();
        Set<SeenItem> lastItems = last.getLastItems();
        final List<T> result = new ArrayList<T>(items.size());
        final int finished = items.size();
        for (int index = 0;
             finished != index;
             index++) {
            final T item = items.get(index);
            final Date itemDateTime = item.getWhenItemChanged();
            final int compare = lastDateTime.compareTo(itemDateTime);
            if (0 < compare) {
                // Skip this item as it should have already been used.
            } else if (0 == compare) {
                final SeenItem testItem = new SeenItem(item.getItem());
                if (!lastItems.contains(testItem)) {
                    result.add(item);
                    if (maxCount == result.size()) {
                        return result;
                    }
                }
            } else {
                // As list is ordered all remaining items will not have been
                // used so can simple finish out the list.
                final int maxFinish;
                if (maxCount > 0) {
                    maxFinish = maxCount - result.size()
                                + index;
                } else {
                    maxFinish = finished;
                }
                if (finished < maxFinish) {
                    result.addAll(items.subList(index,
                                                finished));
                    return result;
                }
                result.addAll(items.subList(index,
                                            maxFinish));
                return result;
            }
        }
        return result;
    }

    // Description of this object.
    // @Override
    // public String toString() {}

}
// public static void main(String args[]) {}