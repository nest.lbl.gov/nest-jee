package gov.lbl.nest.jee.watching;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import gov.lbl.nest.common.watching.ChangedItem;
import gov.lbl.nest.common.watching.WatchedList;

/**
 * This is a simple JEE implementation of the {@link WatchedList} interface.
 * 
 * @author patton
 */
public class WatchedListImpl implements
                             WatchedList {

    /**
     * The {@link WatchedListFactoryImpl} instance that created this Object.
     */
    private WatchedListFactoryImpl factory;

    /**
     * The {@link Callback} instance to be used by this Object to read the contents
     * of the list being watched.
     */
    private final Callback callback;

    /**
     * The unique {@link String} that specifies this Object.
     */
    private final String role;

    /**
     * Creates an instance of this class.
     * 
     * @param factory
     *            the {@link WatchedListFactoryImpl} instance that created this
     *            Object.
     * @param role
     *            the unique {@link String} that specifies this Object.
     * @param callback
     *            the {@link gov.lbl.nest.common.watching.WatchedList.Callback}
     *            instance to be used by this Object to read the contents of the
     *            list being watched.
     */
    public WatchedListImpl(WatchedListFactoryImpl factory,
                           String role,
                           Callback callback) {
        this.callback = callback;
        this.factory = factory;
        this.role = role;
    }

    /**
     * Returns the next block of {@link ChangedItem} instances, with no limit.
     * 
     * This is the same as <code>getNextBlock(null)</code>
     */
    @Override
    public List<? extends ChangedItem> getNextBlock() {
        return getNextBlock(null);
    }

    /**
     * Returns the next block of {@link ChangedItem} instances, but limited to the
     * size specified.
     * 
     * @param blockLimit
     *            the maximum number of {@link ChangedItem} instances to include on
     *            the returned list.
     */
    @Override
    public List<? extends ChangedItem> getNextBlock(Integer blockLimit) {
        final LastWatched last = factory.getLastWatched(role);
        if (null == last) {
            throw new IllegalStateException("The WatchedList for the role \"" + role
                                            + "\" does not exist");
        }
        final Date lastDateTime = last.getLastDateTime();
        final Integer blockLimitToUse;
        if (null == blockLimit) {
            blockLimitToUse = null;
        } else if (null == lastDateTime) {
            /**
             * The first requested block does not have first item removed so simply use the
             * supplied limit.
             */
            blockLimitToUse = blockLimit;
        } else {
            /*
             * Otherwise add one to the requested block limit to account for the first item
             * that will be removed by the `forwardLastWatched` call.
             */
            blockLimitToUse = blockLimit.intValue() + 1;
        }

        final List<? extends ChangedItem> itemsOnOrAfter = callback.getItemsOnOrAfter(lastDateTime,
                                                                                      blockLimitToUse);
        if (null == itemsOnOrAfter) {
            return null;
        }
        final List<? extends ChangedItem> result = factory.forwardLastWatched(last,
                                                                              itemsOnOrAfter);
        return Collections.unmodifiableList(result);
    }

}
