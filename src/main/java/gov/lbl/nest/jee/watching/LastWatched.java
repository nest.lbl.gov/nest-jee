package gov.lbl.nest.jee.watching;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;

/**
 * This class contains information about the last time items for a specified
 * role where watched.
 * 
 * @author patton
 */
@Entity
@NamedQueries({ @NamedQuery(name = "getLastWatched",
                            query = "SELECT m" + " FROM LastWatched m"
                                    + " WHERE m.role = :role") })
public class LastWatched {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * This object's unique identifier within the set of {@link LastWatched}
     * instances.
     */
    private int lastWatchedKey;

    /**
     * The last date and time that in the most recent notification.
     */
    private Date lastDateTime;

    /**
     * The set of items that had the last date and time and were included in the
     * most recent notification.
     */
    private Set<SeenItem> lastItems;

    /**
     * The role of the person or process whose last time watched this object
     * represents.
     */
    private String role;

    // constructors

    /**
     * Create an instance of this class.
     */
    protected LastWatched() {
    }

    /**
     * Create an instance of this class.
     * 
     * @param role
     *            the role of the person or process whose last time watched this
     *            object represents.
     * @param dateTime
     *            the last date and time that in the most recent notification.
     */
    LastWatched(String role,
                Date dateTime) {
        setRole(role);
        setLastDateTime(dateTime);
        setLastItems(new HashSet<SeenItem>());
    }

    // instance member method (alphabetic)

    /**
     * Returns the last date and time that in the most recent notification.
     * 
     * @return the last date and time that in the most recent notification.
     */
    @Column(nullable = false)
    public Date getLastDateTime() {
        return lastDateTime;
    }

    /**
     * Returns the set of items that had the last date and time and were included in
     * the most recent notification.
     * 
     * @return the set of items that had the last date and time and were included in
     *         the most recent notification.
     */
    @OneToMany(mappedBy = "lastWatched",
               cascade = CascadeType.ALL)
    protected Set<SeenItem> getLastItems() {
        return lastItems;
    }

    /**
     * Returns this object's unique identifier within the set of {@link LastWatched}
     * instances.
     * 
     * @return this object's unique identifier within the set of {@link LastWatched}
     *         instances.
     */
    @Id
    @GeneratedValue
    protected int getLastWatchedKey() {
        return lastWatchedKey;
    }

    /**
     * Returns the role of the person or process whose last time watched this object
     * represents.
     * 
     * @return the role of the person or process whose last time watched this object
     *         represents.
     */
    @Column(nullable = false,
            updatable = false)
    public String getRole() {
        return role;
    }

    /**
     * Sets the last date and time that in the most recent notification.
     * 
     * @param lastDateTime
     *            the last date and time that in the most recent notification.
     */
    protected void setLastDateTime(Date lastDateTime) {
        this.lastDateTime = lastDateTime;
    }

    /**
     * Sets the set of items that had the last date and time and were included in
     * the most recent notification.
     * 
     * @param lastItems
     *            the set of items that had the last date and time and were included
     *            in the most recent notification.
     */
    protected void setLastItems(Set<SeenItem> lastItems) {
        this.lastItems = lastItems;
    }

    /**
     * Sets this object's unique identifier within the set of {@link LastWatched}
     * instances.
     * 
     * @param key
     *            the unique value
     */
    protected void setLastWatchedKey(int key) {
        lastWatchedKey = key;
    }

    /**
     * Sets the role of the person or process whose last time watched this object
     * represents.
     * 
     * @param role
     *            the role of the person or process whose last time watched this
     *            object represents.
     */
    protected void setRole(String role) {
        this.role = role;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
