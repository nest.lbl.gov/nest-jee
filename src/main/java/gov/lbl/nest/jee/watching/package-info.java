/**
 * <p>
 * This package provides classes that extend the gov.lbl.nest.common.watching
 * behavior to execute within a J2EE environment.
 * </p>
 * 
 * <h2>Requires:</h2>
 * <ul>
 * <li>{@link java.io}</li>
 * <li>{@link java.net}</li>
 * <li>{@link java.util}</li>
 * <li>{@link jakarta.ejb}</li>
 * <li>{@link jakarta.inject}</li>
 * <li>{@link jakarta.mail}</li>
 * <li>{@link jakarta.persistence}</li>
 * <li>{@link jakarta.ws.rs}</li>
 * <li>{@link jakarta.xml.bind}</li>
 * <li>{@link gov.lbl.nest.common.watching}</li>
 * </ul>
 */
package gov.lbl.nest.jee.watching;
