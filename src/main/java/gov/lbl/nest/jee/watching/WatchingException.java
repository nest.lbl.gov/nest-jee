package gov.lbl.nest.jee.watching;

import jakarta.ejb.ApplicationException;

/**
 * This Exception is thrown when there is an issue watching for or dealing with
 * changes.
 * 
 * @author patton
 */
@ApplicationException(rollback = true)
public class WatchingException extends
                               Exception {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * Used by Serializable.
     */
    private static final long serialVersionUID = 1L;

    // private instance member data

    // constructors

    /**
     * Constructs an instance of this class.
     */
    public WatchingException() {
    }

    /**
     * Constructs an instance of this class.
     * 
     * @param message
     *            the detail message. The detail message is saved for later
     *            retrieval by the {@link Throwable#getMessage()} method.
     */
    public WatchingException(String message) {
        super(message);
    }

    /**
     * Constructs an instance of this class.
     * 
     * @param message
     *            the detail message. The detail message is saved for later
     *            retrieval by the {@link Throwable#getMessage()} method.
     * @param cause
     *            the cause (which is saved for later retrieval by the
     *            {@link Throwable#getCause()} method). (A null value is permitted,
     *            and indicates that the cause is nonexistent or unknown.)
     */
    public WatchingException(String message,
                             Throwable cause) {
        super(message,
              cause);
    }

    /**
     * Constructs an instance of this class.
     * 
     * @param cause
     *            the cause (which is saved for later retrieval by the
     *            {@link Throwable#getCause()} method). (A null value is permitted,
     *            and indicates that the cause is nonexistent or unknown.)
     */
    public WatchingException(Throwable cause) {
        super(cause);
    }

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}

}
