package gov.lbl.nest.jee.configure;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.configure.CustomConfigPath;
import jakarta.mail.Authenticator;
import jakarta.mail.PasswordAuthentication;
import jakarta.mail.Session;

/**
 * This class creates a {@link Session} instance following the standard
 * configuration mechanisms used by NEST.
 * 
 * @author patton
 *
 */
public class Mailer {

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(Mailer.class);

    /**
     * The name of the property containing the email credentials.
     */
    private static final String MAIL_CREDENTIALS_PROPERTY = "mail.credentials";

    /**
     * The name of the properties file contains the mail settings
     */
    private static final String MAIL_PROPERTIES = "mail.properties";

    /**
     * The name of the property containing the email credentials.
     */
    private static final String MAIL_USER_PROPERTY = "mail.user";

    @SuppressWarnings("serial")
    private static final List<String> SENSITIVE_PROPERTIES = new ArrayList<String>() {
        {
            add(MAIL_CREDENTIALS_PROPERTY);
        }
    };
    /**
     * The directory that hold this application's configuration files.
     */
    private File configurationDir;
    /**
     * The {@link Session} to be used by this application.
     */
    private Session session;

    /**
     * True if there has been an attempt to create the {@link Session} instance used
     * by this application.
     */
    private boolean sessionAttempted;

    /**
     * Create an instance of this class.
     * 
     * @param configurationDir
     *            the directory that hold this application's configuration files.
     */
    public Mailer(File configurationDir) {
        this.configurationDir = configurationDir;
    }

    /**
     * Returns the {@link Session} instance built following the standard
     * configuration mechanisms used by NEST. This will only build the
     * {@link Session} the first time it is invoked.
     * 
     * @return the {@link Session} instance built following the standard
     *         configuration mechanisms used by NEST. This will only build the
     *         {@link Session} the first time it is invoked.
     */
    public Session getSession() {
        if (!sessionAttempted) {
            try {
                final File fileToUse;
                if (null == configurationDir) {
                    fileToUse = new File(MAIL_PROPERTIES);
                } else {
                    fileToUse = new File(configurationDir,
                                         MAIL_PROPERTIES);
                }
                final CustomConfigPath customConfigPath = new CustomConfigPath(getClass(),
                                                                               MAIL_PROPERTIES,
                                                                               fileToUse,
                                                                               SENSITIVE_PROPERTIES);
                final Properties props = customConfigPath.getProperties();
                if ((props.stringPropertyNames()).isEmpty()) {
                    LOG.warn("No mail properties where found so no mail will be sent or received");
                } else {
                    final String mailUser = props.getProperty(MAIL_USER_PROPERTY);
                    final String mailPassword = props.getProperty(MAIL_CREDENTIALS_PROPERTY);
                    if (null == mailPassword) {
                        session = Session.getInstance(props);
                    } else {
                        session = Session.getInstance(props,
                                                      new Authenticator() {
                                                          @Override
                                                          protected PasswordAuthentication getPasswordAuthentication() {
                                                              return new PasswordAuthentication(mailUser,
                                                                                                mailPassword);
                                                          }
                                                      });
                    }
                }
            } catch (FileNotFoundException e) {
                session = Session.getInstance(new Properties());
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            sessionAttempted = true;
        }
        return session;
    }

    /**
     * Returns <code>true</code> if this Object has a valid session.
     * 
     * @return <code>true</code> if this Object has a valid session.
     */
    public boolean isValid() {
        getSession();
        return null != session;
    }

}
