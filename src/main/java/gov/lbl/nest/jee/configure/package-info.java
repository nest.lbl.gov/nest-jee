/**
 * <p>
 * This package provides classes that extend the gov.lbl.nest.common.configure
 * behavior to execute within a J2EE environment.
 * </p>
 * 
 * <h2>Requires:</h2>
 * <ul>
 * <li>{@link java.io}</li>
 * <li>{@link java.util}</li>
 * <li>{@link jakarta.inject}</li>
 * <li>{@link jakarta.mail}</li>
 * <li>{@link org.slf4j}</li>
 * <li>{@link gov.lbl.nest.common.configure}</li>
 * </ul>
 */
package gov.lbl.nest.jee.configure;