package gov.lbl.nest.jee.testing;

import java.io.InputStream;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;

/**
 * This class contains utility methods to deal with testing with XML files.
 * 
 * @author patton
 */
public class XmlTestUtility {

    /**
     * Returns an Object instance read from an XML representation of it provided by
     * the specified {@link InputStream}.
     * 
     * @param stream
     *            the {@link InputStream} containing the XML representation.
     * @param clazz
     *            the {@link Class} instance defining the type of Object to be read.
     * @param <T>
     *            the Class of the Object to be read.
     * 
     * @return the Object read from the specified {@link InputStream}.
     * 
     * @throws JAXBException
     *             when there is a problem reading the XML.
     */
    @SuppressWarnings("unchecked")
    public static <T> T read(final InputStream stream,
                             Class<T> clazz) throws JAXBException {
        JAXBContext content = JAXBContext.newInstance(clazz);
        final Unmarshaller unmarshaller = content.createUnmarshaller();
        return (T) unmarshaller.unmarshal(stream);
    }
}
