/**
 * <p>
 * This package provides classes that help with the building unit tests within a
 * J2EE environment.
 * </p>
 * 
 * <h2>Requires:</h2>
 * <ul>
 * <li>{@link java.io}</li>
 * <li>{@link jakarta.xml.bind}</li>
 * </ul>
 */
package gov.lbl.nest.jee.testing;