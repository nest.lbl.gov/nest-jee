package gov.lbl.nest.jee.monitor;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQuery;

/**
 * This class captures the name of an attribute for Points of Interest.
 * 
 * @author patton
 */
@Entity
@NamedQuery(name = "getPoIAttributeName",
            query = "SELECT n" + " FROM PoIAttributeName n"
                    + " WHERE n.value = :name")
public class PoIAttributeName {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * This object's unique identifier within the set of {@link PoIAttributeName}
     * instances.
     */
    private int poiAttributeNameKey;

    /**
     * The name of the attribute.
     */
    private String value;

    // constructors

    /**
     * Create an instance of this class.
     */
    protected PoIAttributeName() {
    }

    /**
     * Create an instance of this class.
     * 
     * @param value
     *            the name of the attribute.
     */
    PoIAttributeName(String value) {
        setValue(value);
    }

    // instance member method (alphabetic)

    /**
     * Returns this object's unique identifier within the set of
     * {@link PoIAttributeName} instances.
     * 
     * @return this object's unique identifier within the set of
     *         {@link PoIAttributeName} instances.
     */
    @Id
    @GeneratedValue
    protected int getPoIAttributeNameKey() {
        return poiAttributeNameKey;
    }

    /**
     * Returns the name of the attribute.
     * 
     * @return the name of the attribute.
     */
    protected String getValue() {
        return value;
    }

    /**
     * Sets this object's unique identifier within the set of
     * {@link PoIAttributeName} instances.
     * 
     * @param key
     *            this object's unique identifier within the set of
     *            {@link PoIAttributeName} instances.
     */
    protected void setPoIAttributeNameKey(int key) {
        poiAttributeNameKey = key;
    }

    /**
     * Sets the name of the attribute.
     * 
     * @param value
     *            the name of the attribute.
     */
    protected void setValue(String value) {
        this.value = value;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
