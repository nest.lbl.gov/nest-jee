package gov.lbl.nest.jee.monitor;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQuery;

/**
 * This class captures one guid of event for Points of Interest.
 * 
 * @author patton
 */
@Entity
@NamedQuery(name = "getPoIGuid",
            query = "SELECT g" + " FROM PoIGuid g"
                    + " WHERE g.value = :guid")
public class PoIGuid {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * This object's unique identifier within the set of {@link PoIGuid} instances.
     */
    private int poiGuidKey;

    /**
     * The value of the guid of event.
     */
    private String value;

    // constructors

    /**
     * Create an instance of this class.
     */
    protected PoIGuid() {
    }

    /**
     * Create an instance of this class.
     * 
     * @param value
     *            the value of the guid of event.
     */
    PoIGuid(String value) {
        setValue(value);
    }

    // instance member method (alphabetic)

    /**
     * Returns this object's unique identifier within the set of {@link PoIGuid}
     * instances.
     * 
     * @return this object's unique identifier within the set of {@link PoIGuid}
     *         instances.
     */
    @Id
    @GeneratedValue
    protected int getPoIGuidKey() {
        return poiGuidKey;
    }

    /**
     * Returns the value of the guid of event.
     * 
     * @return the value of the guid of event.
     */
    protected String getValue() {
        return value;
    }

    /**
     * Sets this object's unique identifier within the set of {@link PoIGuid}
     * instances.
     * 
     * @param key
     *            this object's unique identifier within the set of {@link PoIGuid}
     *            instances.
     */
    protected void setPoIGuidKey(int key) {
        poiGuidKey = key;
    }

    /**
     * Sets the value of the guid of event.
     * 
     * @param value
     *            the value of the guid of event.
     */
    protected void setValue(String value) {
        this.value = value;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
