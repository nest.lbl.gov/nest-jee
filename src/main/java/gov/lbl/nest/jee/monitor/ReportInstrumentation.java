package gov.lbl.nest.jee.monitor;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import gov.lbl.nest.common.monitor.InstrumentationManager;
import gov.lbl.nest.common.monitor.PointOfInterest;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.ResponseBuilder;
import jakarta.ws.rs.core.UriInfo;
import jakarta.xml.bind.DatatypeConverter;

/**
 * The class provides a RESTful interface to access instrumentation information
 * for an application
 * 
 * @author patton
 */
@Path("report/instrumentation")
@Stateless
public class ReportInstrumentation {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The format for date queries.
     */
    private static final String DATE_FORMAT = "yyyy-MM-dd";

    /**
     * The format for date and time queries.
     */
    private static final String DATE_AND_TIME_FORMAT = DATE_FORMAT + "'T'HH:mm:ss";

    /**
     * The format for date and time queries.
     */
    private static final String LONG_DATE_AND_TIME_FORMAT = DATE_FORMAT + "'T'HH:mm:ss.SSS";

    /**
     * The format for date and time queries.
     */
    private static final String ZONED_FORMAT = DATE_AND_TIME_FORMAT + "Z";

    /**
     * The format for date and time queries.
     */
    private static final String LONG_ZONED_FORMAT = LONG_DATE_AND_TIME_FORMAT + "Z";

    /**
     * The formatter for date supplied to this class as Strings.
     */
    private static final DateFormat DATE_FORMATTER = new SimpleDateFormat(DATE_FORMAT);

    /**
     * The formatter for date and time supplied to this class as Strings.
     */
    private static final DateFormat DATE_AND_TIME_FORMATTER = new SimpleDateFormat(DATE_AND_TIME_FORMAT);

    /**
     * The formatter for date and time supplied to this class as Strings.
     */
    private static final DateFormat LONG_DATE_AND_TIME_FORMATTER = new SimpleDateFormat(LONG_DATE_AND_TIME_FORMAT);

    /**
     * The formatter for date and time supplied to this class as Strings.
     */
    private static final DateFormat ZONED_FORMATTER = new SimpleDateFormat(ZONED_FORMAT);

    /**
     * The formatter to use to create a for date and time query element.
     */
    public static final DateFormat LONG_ZONED_FORMATTER = new SimpleDateFormat(LONG_ZONED_FORMAT);

    // private static member data

    // private instance member data

    private static String createJSON(Boolean formatted,
                                     Collection<PointOfInterest> pointsOfInterest,
                                     final Object uri) {
        final String indentToUse;
        final String endOfElement = "\n";
        final String endOfItem;
        final String externalIndent;
        final String separator;
        if (null != formatted && formatted.booleanValue()) {
            indentToUse = "  ";
            endOfItem = "\n";
            externalIndent = indentToUse + indentToUse;
            separator = " : ";
        } else {
            indentToUse = "";
            endOfItem = "";
            externalIndent = null;
            separator = ":";
        }
        final StringBuilder sb = new StringBuilder();
        String conjunction = "";
        sb.append("{" + endOfItem);
        sb.append(indentToUse + "\"uri\""
                  + separator
                  + "\""
                  + uri
                  + "\"");
        if (!pointsOfInterest.isEmpty()) {
            sb.append("," + endOfItem
                      + indentToUse
                      + "\"points\""
                      + separator
                      + "["
                      + endOfElement);
            for (PointOfInterest pointOfInterest : pointsOfInterest) {
                sb.append(conjunction + pointOfInterest.toJSON(externalIndent,
                                                               externalIndent));
                conjunction = "," + endOfElement;
            }
            sb.append(endOfElement + indentToUse
                      + "]");
        }
        sb.append(endOfItem + "}"
                  + endOfItem);
        return sb.toString();
    }

    // constructors

    // instance member method (alphabetic)

    /**
     * Parses a query date and/or time into a {@link Date} instance.
     * 
     * @param dateTime
     *            the date and/or time to parse
     * 
     * @return the {@link Date} instance.
     * 
     * @throws ParseException
     *             when the date and/or time can not be parsed.
     */
    static Date parseQueryDate(final String dateTime) throws ParseException {
        if (null == dateTime) {
            return null;
        }
        try {
            final Calendar calendar = DatatypeConverter.parseDateTime(dateTime);
            return calendar.getTime();
        } catch (IllegalArgumentException e) {

            if (-1 == dateTime.indexOf('.')) {
                if (DATE_FORMAT.length() == dateTime.length()) {
                    return DATE_FORMATTER.parse(dateTime);
                }
                if ((DATE_AND_TIME_FORMAT.length() - 2) == dateTime.length()) {
                    return DATE_AND_TIME_FORMATTER.parse(dateTime);
                }
                return ZONED_FORMATTER.parse(dateTime);
            }
            if ((LONG_DATE_AND_TIME_FORMAT.length() - 2) == dateTime.length()) {
                return LONG_DATE_AND_TIME_FORMATTER.parse(dateTime);
            }
            return LONG_ZONED_FORMATTER.parse(dateTime);
        }
    }

    // static member methods (alphabetic)

    @Inject
    private InstrumentationManager instrumentation;

    /**
     * Returns the JSON representation of the a set of points of interest.
     * 
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param after
     *            the time on or after which bundles should be include in the
     *            returned list.
     * @param before
     *            the time before which bundles should be include in the returned
     *            list. If <code>null</code> then no time limit is used.
     * @param max
     *            the maximum number of bundles to return, 0 or less means
     *            unlimited.
     * @param formatted
     *            true is the results should be formatted.
     * 
     * @return the JSON representation of the a set of points of interest.
     */
    @GET
    @Path("")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response getInstrumentation(@Context UriInfo uriInfo,
                                       @QueryParam("after") String after,
                                       @QueryParam("before") String before,
                                       @QueryParam("max") Integer max,
                                       @QueryParam("formatted") Boolean formatted) {
        ResponseBuilder builder;
        try {
            final Date afterDate = parseQueryDate(after);
            final Date beforeDate = parseQueryDate(before);
            final int maxCount;
            if (null == max) {
                maxCount = 0;
            } else {
                maxCount = max.intValue();
            }
            Collection<PointOfInterest> pointsOfInterest = instrumentation.getPointsOfInterest(afterDate,
                                                                                               beforeDate,
                                                                                               maxCount,
                                                                                               null,
                                                                                               null,
                                                                                               null,
                                                                                               null,
                                                                                               null);
            builder = Response.status(Response.Status.OK);
            final Object uri = ((uriInfo.getBaseUri()).resolve("report/instrumentation")).toString();
            builder = builder.entity(createJSON(formatted,
                                                pointsOfInterest,
                                                uri));
        } catch (ParseException e) {
            builder = Response.status(Response.Status.BAD_REQUEST);
        }
        return builder.build();
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
