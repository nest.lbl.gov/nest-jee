/**
 * This package provides classes that extend the gov.lbl.nest.common.mointor
 * behavior to execute within a J2EE environment.
 * 
 * <h2>Requires:</h2>
 * <ul>
 * <li>{@link java.io}</li>
 * <li>{@link java.text}</li>
 * <li>{@link java.util}</li>
 * <li>{@link jakarta.ejb}</li>
 * <li>{@link jakarta.inject}</li>
 * <li>{@link javax.naming}</li>
 * <li>{@link jakarta.persistence}</li>
 * <li>{@link jakarta.ws.rs}</li>
 * <li>{@link org.slf4j}</li>
 * <li>{@link gov.lbl.nest.common.monitor}</li>
 * </ul>
 * 
 * @author patton
 */
package gov.lbl.nest.jee.monitor;