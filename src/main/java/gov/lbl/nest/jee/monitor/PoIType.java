package gov.lbl.nest.jee.monitor;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQuery;

/**
 * This class captures one type of event for Points of Interest.
 * 
 * @author patton
 */
@Entity
@NamedQuery(name = "getPoIType",
            query = "SELECT t" + " FROM PoIType t"
                    + " WHERE t.value = :type")
public class PoIType {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * This object's unique identifier within the set of {@link PoIType} instances.
     */
    private int poiTypeKey;

    /**
     * The value of the type of event.
     */
    private String value;

    // constructors

    /**
     * Create an instance of this class.
     */
    protected PoIType() {
    }

    /**
     * Create an instance of this class.
     * 
     * @param value
     *            the value of the type of event.
     */
    PoIType(String value) {
        setValue(value);
    }

    // instance member method (alphabetic)

    /**
     * Returns this object's unique identifier within the set of {@link PoIType}
     * instances.
     * 
     * @return this object's unique identifier within the set of {@link PoIType}
     *         instances.
     */
    @Id
    @GeneratedValue
    protected int getPoITypeKey() {
        return poiTypeKey;
    }

    /**
     * Returns the value of the type of event.
     * 
     * @return the value of the type of event.
     */
    protected String getValue() {
        return value;
    }

    /**
     * Sets this object's unique identifier within the set of {@link PoIType}
     * instances.
     * 
     * @param key
     *            this object's unique identifier within the set of {@link PoIType}
     *            instances.
     */
    protected void setPoITypeKey(int key) {
        poiTypeKey = key;
    }

    /**
     * Sets the value of the type of event.
     * 
     * @param value
     *            the value of the type of event.
     */
    protected void setValue(String value) {
        this.value = value;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
