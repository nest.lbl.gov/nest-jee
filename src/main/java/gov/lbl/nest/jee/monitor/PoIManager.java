package gov.lbl.nest.jee.monitor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import gov.lbl.nest.common.monitor.Level;
import jakarta.ejb.AccessTimeout;
import jakarta.ejb.Lock;
import jakarta.ejb.LockType;
import jakarta.ejb.Singleton;
import jakarta.ejb.TransactionAttribute;
import jakarta.ejb.TransactionAttributeType;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.NoResultException;
import jakarta.persistence.TypedQuery;

/**
 * The class manages the {@link PoI} instances from a
 * {@link InstrumentationManagerImpl} instance.
 * 
 * @author patton
 */
@Singleton
@AccessTimeout(value = 120000)
public class PoIManager {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link EntityManager} instance used by this object.
     */
    @Inject
    @InstrumentationDB
    private EntityManager entityManager;

    // constructors

    // instance member method (alphabetic)

    /**
     * Creates a new {@link PoIAttributeName} instance, if it does not already
     * exist, within its own transaction.
     * 
     * @param value
     *            the value of the {@link PoIAttributeName} instance to create.
     * 
     * @return the created {@link PoIAttributeName} instance.
     */
    @Lock(LockType.WRITE)
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public PoIAttributeName createAttributeName(String value) {
        if (null == value) {
            return null;
        }
        final TypedQuery<PoIAttributeName> query = entityManager.createNamedQuery("getPoIAttributeName",
                                                                                  PoIAttributeName.class);
        query.setParameter("name",
                           value);
        try {
            return query.getSingleResult();
        } catch (NoResultException e) {
            PoIAttributeName attributeName = new PoIAttributeName(value);
            entityManager.persist(attributeName);
            return attributeName;
        }
    }

    /**
     * Creates a new {@link PoIGuid} instance, if it does not already exist, within
     * its own transaction.
     * 
     * @param value
     *            the value of the {@link PoIGuid} instance to create.
     * 
     * @return the created {@link PoIGuid} instance.
     */
    @Lock(LockType.WRITE)
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public PoIGuid createGuid(String value) {
        if (null == value) {
            return null;
        }
        final TypedQuery<PoIGuid> query = entityManager.createNamedQuery("getPoIGuid",
                                                                         PoIGuid.class);
        query.setParameter("guid",
                           value);
        try {
            return query.getSingleResult();
        } catch (NoResultException e) {
            PoIGuid guid = new PoIGuid(value);
            entityManager.persist(guid);
            return guid;
        }
    }

    /**
     * Creates a new {@link PoI} instance.
     * 
     * @param dateTime
     *            the date and time when the point of interest occurred.
     * @param type
     *            the type of event that occurred at this point of interest.
     * @param level
     *            the level of detail of this point of interest.
     * @param status
     *            The status, if any, of an operation executed this point of
     *            interest.
     * @param guid
     *            the guid associated of event that occurred at this point of
     *            interest.
     * 
     * @return the create {@link PoI} instance.
     */
    @Lock(LockType.WRITE)
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public PoI createPoI(Date dateTime,
                         PoIType type,
                         Level level,
                         Integer status,
                         PoIGuid guid) {
        final PoIType localType;
        if (null == type) {
            localType = null;
        } else {
            localType = entityManager.merge(type);
        }
        final PoIGuid localGuid;
        if (null == guid) {
            localGuid = null;
        } else {
            localGuid = entityManager.merge(guid);
        }

        final PoI pointOfInterest = new PoI(dateTime,
                                            localType,
                                            level,
                                            status,
                                            localGuid);
        entityManager.persist(pointOfInterest);
        return pointOfInterest;
    }

    /**
     * Creates a new {@link PoIType} instance, if it does not already exist, within
     * its own transaction.
     * 
     * @param value
     *            the value of the {@link PoIType} instance to create.
     * 
     * @return the created {@link PoIType} instance.
     */
    @Lock(LockType.WRITE)
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public PoIType createType(String value) {
        if (null == value) {
            return null;
        }
        final TypedQuery<PoIType> query = entityManager.createNamedQuery("getPoIType",
                                                                         PoIType.class);
        query.setParameter("type",
                           value);
        try {
            return query.getSingleResult();
        } catch (NoResultException e) {
            PoIType type = new PoIType(value);
            entityManager.persist(type);
            return type;
        }
    }

    /**
     * Sets the attributes of the supplied PoI instance.
     * 
     * @param pointOfInterest
     *            the {@link PoI} instance to which the attributes should be
     *            associated.
     * @param names
     *            the {@link PoIAttributeName} instances used by the attributes.
     * @param attributes
     *            the {@link PoIAttribute} instance to associate.
     */
    @Lock(LockType.WRITE)
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void setAttributes(PoI pointOfInterest,
                              Map<String, PoIAttributeName> names,
                              Map<String, String> attributes) {
        final PoI localPoI = entityManager.merge(pointOfInterest);
        final Collection<PoIAttribute> poiAttributes = new ArrayList<PoIAttribute>();
        for (String name : attributes.keySet()) {
            final PoIAttribute attribute = new PoIAttribute(pointOfInterest,
                                                            entityManager.merge(names.get(name)),
                                                            attributes.get(name));
            entityManager.persist(attribute);
            poiAttributes.add(attribute);
        }
        localPoI.setAttributes(poiAttributes);

    }
    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
