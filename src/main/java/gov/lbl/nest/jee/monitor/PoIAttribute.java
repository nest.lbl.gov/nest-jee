package gov.lbl.nest.jee.monitor;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;

/**
 * This class captures the name/value pair of an attribute for Points of
 * Interest.
 * 
 * @author patton
 */
@Entity
public class PoIAttribute {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * This object's unique identifier within the set of {@link PoIAttribute}
     * instances.
     */
    private int poiAttributeKey;

    /**
     * The {@link PoIAttributeName} instance containing the name of the attribute.
     */
    private PoIAttributeName name;

    /**
     * The {@link PoI} to which this attribute belongs.
     */
    private PoI pointOfInterest;

    /**
     * The value of the attribute.
     */
    private String value;

    // constructors

    /**
     * Create an instance of this class.
     */
    protected PoIAttribute() {
    }

    /**
     * Create an instance of this class.
     * 
     * @param pointOfInterest
     *            the {@link PoI} to which this attribute belongs.
     * @param name
     *            the {@link PoIAttributeName} instance containing the name of the
     *            attribute.
     * @param value
     *            the value of the attribute.
     */
    PoIAttribute(PoI pointOfInterest,
                 PoIAttributeName name,
                 String value) {
        setName(name);
        setPointOfInterest(pointOfInterest);
        setValue(value);
    }

    // instance member method (alphabetic)

    /**
     * Returns the {@link PoIAttributeName} instance containing the name of the
     * attribute.
     * 
     * @return the {@link PoIAttributeName} instance containing the name of the
     *         attribute.
     */
    @ManyToOne
    protected PoIAttributeName getName() {
        return name;
    }

    /**
     * Returns this object's unique identifier within the set of
     * {@link PoIAttribute} instances.
     * 
     * @return this object's unique identifier within the set of
     *         {@link PoIAttribute} instances.
     */
    @Id
    @GeneratedValue
    protected int getPoIAttributeKey() {
        return poiAttributeKey;
    }

    /**
     * Returns the {@link PoI} to which this attribute belongs.
     * 
     * @return the {@link PoI} to which this attribute belongs.
     */
    @ManyToOne
    protected PoI getPointOfInterest() {
        return pointOfInterest;
    }

    /**
     * Returns the value of the attribute.
     * 
     * @return the value of the attribute.
     */
    protected String getValue() {
        return value;
    }

    /**
     * Sets the {@link PoIAttributeName} instance containing the name of the
     * attribute.
     * 
     * @param name
     *            the {@link PoIAttributeName} instance containing the name of the
     *            attribute.
     */
    protected void setName(PoIAttributeName name) {
        this.name = name;
    }

    /**
     * Sets this object's unique identifier within the set of {@link PoIAttribute}
     * instances.
     * 
     * @param key
     *            this object's unique identifier within the set of
     *            {@link PoIAttribute} instances.
     */
    protected void setPoIAttributeKey(int key) {
        poiAttributeKey = key;
    }

    /**
     * Sets the {@link PoI} to which this attribute belongs.
     * 
     * @param pointOfInterest
     *            the {@link PoI} to which this attribute belongs.
     */
    protected void setPointOfInterest(PoI pointOfInterest) {
        this.pointOfInterest = pointOfInterest;
    }

    /**
     * Sets the value of the attribute.
     * 
     * @param value
     *            the value of the attribute.
     */
    protected void setValue(String value) {
        this.value = value;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
