package gov.lbl.nest.jee.monitor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import gov.lbl.nest.common.monitor.InstrumentationManager;
import gov.lbl.nest.common.monitor.Level;
import gov.lbl.nest.common.monitor.PointOfInterest;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.NoResultException;
import jakarta.persistence.TypedQuery;

/**
 * This class is a default {@link InstrumentationManager}, bound the
 * InstrumentationDB.
 * 
 * @author patton
 */
@Stateless
public class InstrumentationManagerImpl implements
                                        InstrumentationManager {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    /**
     * The {@link Lock} used to ensure only single instances of a {@link PoIGuid}
     * are created.
     */
    private static Lock GUID_LOCK = new ReentrantLock();

    /**
     * The {@link Lock} used to ensure only single instances of a
     * {@link PoIAttributeName} are created.
     */
    private static Lock NAME_LOCK = new ReentrantLock();

    /**
     * The {@link Lock} used to ensure only single instances of a {@link PoIType}
     * are created.
     */
    private static Lock TYPE_LOCK = new ReentrantLock();

    // private instance member data

    /**
     * The {@link EntityManager} instance used by this object.
     */
    @Inject
    @InstrumentationDB
    private EntityManager entityManager;

    /**
     * The {@link PoIManager} instance used by this object.
     */
    @Inject
    private PoIManager poiManager;

    // constructors

    // instance member method (alphabetic)

    private PoIAttributeName getPoIAttributeName(String name) {
        if (null == name) {
            return null;
        }
        final TypedQuery<PoIAttributeName> query = entityManager.createNamedQuery("getPoIAttributeName",
                                                                                  PoIAttributeName.class);
        query.setParameter("name",
                           name);
        NAME_LOCK.lock();
        try {
            try {
                return query.getSingleResult();
            } catch (NoResultException e) {
                return poiManager.createAttributeName(name);
            }
        } finally {
            NAME_LOCK.unlock();
        }
    }

    private PoIGuid getPoIGuid(String guid) {
        if (null == guid) {
            return null;
        }
        final TypedQuery<PoIGuid> query = entityManager.createNamedQuery("getPoIGuid",
                                                                         PoIGuid.class);
        query.setParameter("guid",
                           guid);
        GUID_LOCK.lock();
        try {
            try {
                return query.getSingleResult();
            } catch (NoResultException e) {
                return poiManager.createGuid(guid);
            }
        } finally {
            GUID_LOCK.unlock();
        }
    }

    @Override
    public Collection<PointOfInterest> getPointsOfInterest(Date since,
                                                           Date before,
                                                           int maxCount,
                                                           String type,
                                                           Level level,
                                                           Integer status,
                                                           String guid,
                                                           Map<String, String> attributes) {
        final TypedQuery<PoI> query;
        if (null == before) {
            query = entityManager.createNamedQuery("pointsSince",
                                                   PoI.class);
        } else {
            query = entityManager.createNamedQuery("pointsBetween",
                                                   PoI.class);
            query.setParameter("before",
                               before);
        }
        if (null == since) {
            query.setParameter("since",
                               new Date(0));
        } else {
            query.setParameter("since",
                               since);
        }
        if (maxCount > 0) {
            query.setMaxResults(maxCount);
        }
        final List<PoI> pointsOfInterest = query.getResultList();
        final List<PointOfInterest> result = new ArrayList<PointOfInterest>(pointsOfInterest.size());
        if (pointsOfInterest.isEmpty()) {
            return result;
        }
        for (PoI pointOfInterest : pointsOfInterest) {
            final PoIType poIType = pointOfInterest.getType();
            final String typeToCovert;
            if (null == poIType) {
                typeToCovert = null;
            } else {
                typeToCovert = poIType.getValue();
            }
            final PoIGuid poIGuid = pointOfInterest.getGuid();
            final String guidToCovert;
            if (null == poIGuid) {
                guidToCovert = null;
            } else {
                guidToCovert = poIGuid.getValue();
            }
            final Collection<PoIAttribute> attributesToCovert = pointOfInterest.getAttributes();
            final Map<String, String> attributesToUse;
            if (null != attributesToCovert && !attributesToCovert.isEmpty()) {
                attributesToUse = new HashMap<String, String>();
                for (PoIAttribute attribute : attributesToCovert) {
                    attributesToUse.put((attribute.getName()).getValue(),
                                        attribute.getValue());
                }
            } else {
                attributesToUse = null;
            }
            result.add(new PointOfInterest(pointOfInterest.getWhenOccurred(),
                                           typeToCovert,
                                           pointOfInterest.getLevel(),
                                           pointOfInterest.getStatus(),
                                           guidToCovert,
                                           attributesToUse));
        }
        return result;
    }

    private PoIType getPoIType(String type) {
        final TypedQuery<PoIType> query = entityManager.createNamedQuery("getPoIType",
                                                                         PoIType.class);
        query.setParameter("type",
                           type);
        try {
            return query.getSingleResult();
        } catch (NoResultException e) {
            TYPE_LOCK.lock();
            try {
                return poiManager.createType(type);
            } finally {
                TYPE_LOCK.unlock();
            }
        }
    }

    @Override
    public void pointOfInterest(Date dateTime,
                                String type,
                                Level level,
                                Integer status,
                                String guid,
                                Map<String, String> attributes) {

        final PoI pointOfInterest = poiManager.createPoI(dateTime,
                                                         getPoIType(type),
                                                         level,
                                                         status,
                                                         getPoIGuid(guid));
        if (null != attributes) {
            final Map<String, PoIAttributeName> names = new HashMap<String, PoIAttributeName>();
            for (String name : attributes.keySet()) {
                names.put(name,
                          getPoIAttributeName(name));
            }
            poiManager.setAttributes(pointOfInterest,
                                     names,
                                     attributes);
        }
    }
    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
