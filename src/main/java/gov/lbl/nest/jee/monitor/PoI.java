package gov.lbl.nest.jee.monitor;

import java.util.Collection;
import java.util.Date;

import gov.lbl.nest.common.monitor.Level;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;

/**
 * The class save a point of interest.
 * 
 * @author patton
 */
@Entity
@NamedQueries({ @NamedQuery(name = "pointsSince",
                            query = "SELECT p" + " FROM PoI p"
                                    + " WHERE p.whenOccurred IS NOT NULL"
                                    + " AND p.whenOccurred >= :since"
                                    + " ORDER BY p.whenOccurred ASC"),
                @NamedQuery(name = "pointsBetween",
                            query = "SELECT p" + " FROM PoI p"
                                    + " WHERE p.whenOccurred IS NOT NULL"
                                    + " AND p.whenOccurred >= :since"
                                    + " AND p.whenOccurred < :before"
                                    + " ORDER BY p.whenOccurred ASC") })
public class PoI {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The other attributes of this point of interest.
     */
    private Collection<PoIAttribute> attributes;

    /**
     * The guid associated of event that occurred at this point of interest.
     */
    private PoIGuid guid;

    /**
     * The level of detail of this point of interest.
     */
    private Level level;

    /**
     * This object's unique identifier within the set of {@link PoI} instances.
     */
    private int pointOfInterestKey;

    /**
     * The status, if any, of an operation executed this point of interest.
     */
    private Integer status;

    /**
     * The type of event that occurred at this point of interest.
     */
    private PoIType type;

    /**
     * The date and time when the point of interest occurred.
     */
    private Date whenOccurred;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected PoI() {
    }

    /**
     * Creates an instance of this class.
     * 
     * @param dateTime
     *            the date and time when the point of interest occurred.
     * @param type
     *            the type of event that occurred at this point of interest.
     * @param level
     *            the level of detail of this point of interest.
     * @param status
     *            The status, if any, of an operation executed this point of
     *            interest.
     * @param guid
     *            the guid associated of event that occurred at this point of
     *            interest.
     */
    PoI(Date dateTime,
        PoIType type,
        Level level,
        Integer status,
        PoIGuid guid) {
        setGuid(guid);
        setLevel(level);
        setStatus(status);
        setType(type);
        setWhenOccurred(dateTime);
    }

    // instance member method (alphabetic)

    /**
     * Returns the other attributes of this point of interest.
     * 
     * @return the other attributes of this point of interest.
     */
    @OneToMany(mappedBy = "pointOfInterest",
               cascade = CascadeType.ALL)
    protected Collection<PoIAttribute> getAttributes() {
        return attributes;
    }

    /**
     * Returns the guid associated of event that occurred at this point of interest.
     * 
     * @return the guid associated of event that occurred at this point of interest.
     */
    @ManyToOne
    protected PoIGuid getGuid() {
        return guid;
    }

    /**
     * Returns the level of detail of this point of interest.
     * 
     * @return the level of detail of this point of interest.
     */
    @Enumerated
    protected Level getLevel() {
        return level;
    }

    /**
     * Returns this object's unique identifier within the set of {@link PoI}
     * instances.
     * 
     * @return this object's unique identifier within the set of {@link PoI}
     *         instances.
     */
    @Id
    @GeneratedValue
    protected int getPointOfInterestKey() {
        return pointOfInterestKey;
    }

    /**
     * Returns the status, if any, of an operation executed this point of interest.
     * 
     * @return the status, if any, of an operation executed this point of interest.
     */
    protected Integer getStatus() {
        return status;
    }

    /**
     * Return the type of event that occurred at this point of interest.
     * 
     * @return the type of event that occurred at this point of interest.
     */
    @ManyToOne
    protected PoIType getType() {
        return type;
    }

    /**
     * Returns the date and time when the point of interest occurred.
     * 
     * @return the date and time when the point of interest occurred.
     */
    @Temporal(TemporalType.TIMESTAMP)
    protected Date getWhenOccurred() {
        return whenOccurred;
    }

    /**
     * Sets the other attributes of this point of interest.
     * 
     * @param attributes
     *            the other attributes of this point of interest.
     */
    protected void setAttributes(Collection<PoIAttribute> attributes) {
        this.attributes = attributes;
    }

    /**
     * Sets the guid associated of event that occurred at this point of interest.
     * 
     * @param guid
     *            the guid associated of event that occurred at this point of
     *            interest.
     */
    protected void setGuid(PoIGuid guid) {
        this.guid = guid;
    }

    /**
     * Sets the level of detail of this point of interest.
     * 
     * @param level
     *            the level of detail of this point of interest.
     */
    protected void setLevel(Level level) {
        this.level = level;
    }

    /**
     * Sets this object's unique identifier within the set of {@link PoI} instances.
     * 
     * @param key
     *            this object's unique identifier within the set of {@link PoI}
     *            instances.
     */
    protected void setPointOfInterestKey(int key) {
        pointOfInterestKey = key;
    }

    /**
     * Sets the status, if any, of an operation executed this point of interest.
     * 
     * @param status
     *            the status, if any, of an operation executed this point of
     *            interest.
     */
    protected void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * Sets the type of event that occurred at this point of interest.
     * 
     * @param type
     *            the type of event that occurred at this point of interest.
     */
    protected void setType(PoIType type) {
        this.type = type;
    }

    /**
     * Sets the date and time when the point of interest occurred.
     * 
     * @param dateTime
     *            the date and time when the point of interest occurred.
     */
    protected void setWhenOccurred(Date dateTime) {
        whenOccurred = dateTime;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
