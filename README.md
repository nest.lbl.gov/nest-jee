# Welcome to `nest-common` #

## Overview ##

[nest-jee](https://gitlab.com/nest.lbl.gov/nest-jee) contains
classes that depend on the JEE API and that are common among the NEST
projects.

Currently this project includes the classes dealing with the following
topics, built on the foundation created by
[nest-common](https://gitlab.com/nest.lbl.gov/nest-common):

*    Digest: A set of classes used to provide a summary of changes to a
set of "Watched" items.

*    Instrumentation: A set of classes used to provide instrumentation
of NEST projects.

*    Configuration: classes used to manage configurations of
applications.
