# Developer Notes for `nest-jee` #

These notes are to help developers develop, maintain and debug the `nest-jee` Java library.


# Index of Notes #

Below is a list of notes. They are in no particular order (as defining one and maintaining it does not seem the best use of time).

*   [Building `nest-jee`](#building-nest-jee)
*   [Deploying `nest-jee`](#deploying-nest-jee)


# Building `nest-jee` #

The following assumes [maven](http://maven.apache.org/) is already installed and shows how to build and install a released version of `nest-jee` locally.

    NEST-JEE_VERSION=1.9.1
    git clone git@gitlab.com:nest.lbl.gov/nest-jee.git
    cd nest-jee
    git checkout ${NEST-JEE_VERSION}
    mvn clean install

Clearly to build the `master` the `NEST-JEE_VERSION` should be set to that value.

---
_Note:_ This step assumes all of the JAR files on which `nest-jee` depends are available from their maven repositories, if not the following links show how to build them.

*   [`nest-common`](https://gitlab.com/nest.lbl.gov/nest-common/-/blob/master/developer_notes.md#building-nest-common)
---


# Deploying `nest-jee` #

A locally installed `nest-jee` application can be deployed to its maven respository using the following command.

    mvn deploy

For this to work transparently, the credentials, i.e. ssh key and config, that allow the user `nest` to log into `nest.lbl.gov` must be correctly configured, and the `~/.m2/settings.xml` file should contain an element like the following to set up the user name to be used to deploy to the `nest-maven2-projects` repository.


    <settings>
        ...
        <servers>
            ...
            <server>
                <id>nest-maven2-projects</id>
                <username>nest</username>
            </server>
            ...
        </servers>
        ...
    </settings>
